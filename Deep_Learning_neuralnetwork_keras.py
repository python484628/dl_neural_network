
# Author : Marion Estoup
# E-mail : marion_110@hotmail.fr
# May 2023


# Deep Learning with Keras


# First neural network with keras tutorial

# Load libraries
from keras.models import Sequential
from keras.layers import Dense
from tensorflow.keras.utils import to_categorical
from sklearn import datasets

# Load the dataset
dataset = datasets.load_digits()


# Define X and y 
# In load_digits, y is a vector with all class values
# In the neural network we represent one vector (if we have 10 classes then we'll have 10 neurons in the output layer
# with each neuron that represents one class)
# For exemple if class 0 then there is 1 in the first layer and 0 in all the other layers
# If class 1 then there is 1 in the second layer and 0 in all the other layers
# Etc
# to_categorical allows to retrieve value, number of possible class, ...
X = dataset.data
y = to_categorical(dataset.target)
print(X.shape) # (1797, 64)
print(y.shape) # (1797, 10)

# Only quantitative values for the class
# If binary, there is only one neuron (then no need of the vector with to_categorical) but here it's muulticlass


# Define the keras model 

# Here we can use a different function for each layer if we want to
# Which is not possible with scikit learn where the function should be the same for each layer
model = Sequential()
model.add(Dense(100, input_dim=X.shape[1], activation='tanh'))
model.add(Dense(20, activation='relu'))


# if y shape smaller or equal to 2 then we take the number of values that we have in y (size)
if (y.shape[1]<=2):
    size=1
else:
    size=y.shape[1]

# Output layer
model.add(Dense(size,activation='softmax')) 

# We define the size/dimension of the input data for the first hidden layer
# If y not smaller or equal to 2, the first layer will have the same size/dimension as the input data
# Number of attributes that we have in our data = X.shape[1], so we know the size of the hidden layer



# Compile the keras model
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])


# Fit the leras model on the dataset 
model.fit(X,y,epochs=300, batch_size=100, verbose=1)
# We see that the cost is decreasing with the epochs
# And we have an accuracy of 1 (100%) quickly (around 30-40 epochs)

# We estimate the cost on the training dataset but we also have to look the test dataset to know if there is overfitting




# Show model summary
model.summary()
# Small model, 8730 parameters (weight that we calibrated with the small model)


# print model accuracy and loss
print(model.history.history["loss"])
print(model.history.history["accuracy"])





# Import libraries
import seaborn as sns
import pandas as pd

# Create dataframe
df = pd.DataFrame.from_dict(model.history.history)
df.head()

# Plot the accuracy with seaborn
sns.lineplot(data=df["accuracy"])


# Plot the loss and accuracy
sns.lineplot(data=df)




# Evaluate the keras model 
_, accuracy = model.evaluate(X,y)
# _ is like anonymous variable, here we collect and store only the accuracy (second value) and not the first value 

print('Accuracy: %.2f' % (accuracy*100))
# % to correspond to variables
# .2f, f for float (if d it's decimal for example)
# We retrieve 2 values 

 


# Make predictions with the model 
proba_predictions = model.predict(X)
print(proba_predictions)
# Give score for each neuron
# We have 10 neurons so 10 classes, that's why we have a vector size 10
# Weak value because power -1
